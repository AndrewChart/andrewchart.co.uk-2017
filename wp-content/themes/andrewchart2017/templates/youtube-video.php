<iframe 
    src="https://www.youtube.com/embed/<?= $yt_id ?>?enablejsapi=1&origin=<?= get_site_url() ?>" 
    class="youtube-embed"
    frameborder=0
    title="YouTube video player"
    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" 
    allowfullscreen>
</iframe>